$.validator.setDefaults({
    submitHandler: function () {
        alert("submitted!");
    }
});

$(function () {

    $("#company-detail").validate({
        errorContainer: $('.validatorCalloutHighlight'),
        errorLabelContainer: $('.validatorCalloutHighlight ul'),
        errorElement: 'li',
        rules: {
            "companyName": {
                required: true,
                remote: "http://localhost:8080/validation"
            },
            "registeredCountry": "required"
        }
        ,
        messages: {
            "companyName": {
                required: "Please enter your Company name",
                remote: "Already in use"
            },
            "registeredCountry": "Please select country where company is registered"
        }
        
    });

});